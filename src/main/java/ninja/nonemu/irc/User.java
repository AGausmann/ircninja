/*
 * ircninja
 * Copyright (C) 2016  Adam Gausmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ninja.nonemu.irc;

/**
 * Represents a user that is or was connected to an IRC network.
 *
 * For more detailed information about users who are currently online, use the {@link OnlineUser} class.
 *
 * @see OnlineUser
 * @see Connection#getUser(String)
 */
public class User implements ChatSender, ChatRecipient {

    private final Connection connection;
    private final String nick;
    private final String user;
    private final String host;
    private final String realname;
    private final String server;
    private final boolean online;

    /**
     * Constructs a new User.
     * @param connection The connection that the user originate from.
     * @param nick The nickname of the user.
     * @param user The username of the user.
     * @param host The hostname of the user.
     * @param realname The real name of the user.
     * @param server The server the user is/was connected to.
     * @param online Whether the user is currently online.
     */
    public User(Connection connection, String nick, String user, String host, String realname, String server, boolean online) {
        this.connection = connection;
        this.nick = nick;
        this.user = user;
        this.host = host;
        this.realname = realname;
        this.server = server;
        this.online = online;
    }

    public String getNick() {
        return nick;
    }

    public String getUser() {
        return user;
    }

    public String getHost() {
        return host;
    }

    public String getRealname() {
        return realname;
    }

    public String getServer() {
        return server;
    }

    public boolean isOnline() {
        return online;
    }

    @Override
    public String getName() {
        return nick;
    }

    @Override
    public void sendChat(String message) {
        connection.sendChat(nick, message);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user1 = (User) o;

        return connection.equals(user1.connection)
                && nick.equals(user1.nick)
                && user.equals(user1.user)
                && host.equals(user1.host);
    }

    @Override
    public int hashCode() {
        int result = connection.hashCode();
        result = 31 * result + nick.hashCode();
        result = 31 * result + user.hashCode();
        result = 31 * result + host.hashCode();
        return result;
    }
}
