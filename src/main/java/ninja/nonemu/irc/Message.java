/*
 * ircninja
 * Copyright (C) 2016  Adam Gausmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ninja.nonemu.irc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Represents a message sent between server and client.
 *
 * <br>According to RFC 2812, a message consists of an optional prefix, a command, and up to 15 arguments, the last of
 * which may contain spaces. The argument with spaces is denoted as the suffix in this class, and is optional.
 */
public class Message {
    private final String prefix;
    private final String command;
    private final String[] args;

    /**
     * Constructs a new message instance from raw components.
     * @param prefix The prefix of the message, may be null.
     * @param command The command of the message, may not be null.
     * @param args The arguments of the message, may be null or from 0 up to 15. The last argument may contain
     *             whitespace.
     */
    public Message(String prefix, String command, String[] args) {
        this.prefix = prefix;
        this.command = command;
        this.args = args;
    }

    /**
     * Validates the message using rules taken from RFC 2812, section 2.3.
     * @return true if the message is valid, false otherwise.
     */
    public boolean isValid() {
        if (prefix != null && prefix.contains(" ")) {
            return false;
        }
        if (command == null) {
            return false;
        }
        if (args != null) {
            if (args.length > 15) {
                return false;
            }
            for (int i = 0; i < args.length; i++) {
                if (args[i] == null || i != args.length - 1 && args[i].contains(" ") || args[i].isEmpty()) {
                    return false;
                }
            }
        }
        if (prefix != null && prefix.isEmpty()) {
            return false;
        }
        if (encode().length() > 512) {
            return false;
        }
        return true;
    }

    /**
     * Encodes the message into the format that is sent over the socket, not including the trailing CRLF.
     * @return The String-encoded message.
     */
    public String encode() {
        StringBuilder result = new StringBuilder();

        if (prefix != null) {
            result.append(":").append(prefix).append(" ");
        }

        result.append(command);

        if (args != null) {
            for (int i = 0; i < args.length; i++) {
                if (i == args.length - 1 && args[i].contains(" ")) {
                    result.append(":");
                }
                result.append(" ").append(args[i]);
            }
        }

        return result.toString();
    }

    /**
     * Extracts the name of the sender from the prefix of the message.
     *
     * <br>The prefix MUST NOT BE NULL, otherwise a NullPointerException will be thrown.
     */
    public String senderName() {
        if (prefix.contains("!")) {
            return prefix.substring(0, prefix.indexOf("!"));
        }
        return prefix;
    }

    /**
     * Extracts the username of the sender from the prefix of the message. This requires the sender to be a user
     * (not a server) in order for the command to work.
     *
     * <br>The prefix MUST NOT BE NULL, otherwise a NullPointerException will be thrown.
     */
    public String senderUser() {
        if (prefix.contains("@")) {
            return prefix.substring(prefix.indexOf("!") + 1, prefix.indexOf("@"));
        }
        return prefix.substring(prefix.indexOf("!") + 1);
    }

    /**
     * Extracts the hostname of the sender from the prefix of the message. This requires the sender to be a user
     * (not a server) in order for the command to work. If you wish to get the hostname of a server who sent a message,
     * you can use the {@link #senderName()} or {@link #getPrefix()} method.
     *
     * <br> The prefix MUST NOT BE NULL, otherwise a NullPointerException will be thrown.
     */
    public String senderHost() {
        if (prefix.contains("@")) {
            return prefix.substring(prefix.indexOf("@") + 1);
        }
        return null;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getCommand() {
        return command;
    }

    public String[] getArgs() {
        return args;
    }

    public String getArg(int i) {
        return args[i];
    }

    public int getArgCount() {
        return args.length;
    }

    @Override
    public String toString() {
        return encode();
    }

    /**
     * Decodes a message from its encoded form.
     * @param s The encoded message, not including the trailing CRLF.
     * @return An instance representing the message.
     */
    public static Message decode(String s) {
        String prefix = null;
        String command;
        String[] args;
        String suffix = null;

        List<String> words = new ArrayList<>(Arrays.asList(s.split(" ")));
        if (words.get(0).startsWith(":")) {
            prefix = words.remove(0).substring(1);
        }

        command = words.remove(0);

        List<String> argList = new LinkedList<>();
        while (!words.isEmpty()) {
            if (words.get(0).startsWith(":")) {
                argList.add(String.join(" ", words).substring(1));
                break;
            }
            argList.add(words.remove(0));
        }
        args = new String[argList.size()];
        argList.toArray(args);

        return new Message(prefix, command, args);
    }
}
