/*
 * ircninja
 * Copyright (C) 2016  Adam Gausmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ninja.nonemu.irc;

/**
 * This class provides a safe way to construct IRC connections. It checks to make sure that all of the necessary fields
 * are filled out before constructing the connection.
 *
 * @see Connection
 */
public class ConnectionBuilder {
    private String host;
    private int port;
    private String nickname;
    private String username;
    private String realname;
    private String password;

    /**
     * Constructs a new connection builder using default values
     * <br>hostname: null*
     * <br>port: 6667
     * <br>nickname: null*
     * <br>username: system username*
     * <br>realname: IRCNinja message*
     * <br>password: null
     * <br><i>*: Cannot be null when building</i>
     */
    public ConnectionBuilder() {
        host = null;
        port = 6667;
        nickname = null;
        username = System.getProperty("user.name");
        realname = "Client using the IRCNinja library for Java";
        password = null;
    }

    public void host(String host) {
        this.host = host;
    }

    public void port(int port) {
        this.port = port;
    }

    public void nickname(String nickname) {
        this.nickname = nickname;
    }

    public void username(String username) {
        this.username = username;
    }

    public void realname(String realname) {
        this.realname = realname;
    }

    public void password(String password) {
        this.password = password;
    }

    /**
     * Builds the connection instance.
     */
    public Connection build() {
        if (host == null) {
            throw new IllegalArgumentException("Hostname cannot be null");
        }
        if (nickname == null) {
            throw new IllegalArgumentException("Nickname cannot be null");
        }
        if (username == null) {
            throw new IllegalArgumentException("Username cannot be null");
        }
        if (realname == null) {
            throw new IllegalArgumentException("Real name cannot be null");
        }
        if (!IrcUtil.isNickname(nickname)) {
            throw new IllegalArgumentException("Nickname contains illegal characters.");
        }
        if (!IrcUtil.isUsername(username)) {
            throw new IllegalArgumentException("Username contains illegal characters.");
        }
        if (IrcUtil.isUnsafe(realname)) {
            throw new IllegalArgumentException("Real name contains illegal characters.");
        }
        if (password != null && IrcUtil.isUnsafe(password)) {
            throw new IllegalArgumentException("Password contains illegal characters.");
        }

        return new Connection(host, port, nickname, username, realname, password);
    }
}
