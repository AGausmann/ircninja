/*
 * ircninja
 * Copyright (C) 2016  Adam Gausmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ninja.nonemu.irc;

/**
 * Represents a channel on an IRC network where multiple users can chat together.
 * @see Connection#getChannel(String)
 */
public class Channel implements ChatRecipient, ChatSender {

    private final Connection connection;
    private final String name;
    private final String topic;
    private final String[] users;

    /**
     * Constructs a new Channel.
     * @param connection The connection that the channel originated from.
     * @param name The name of the channel.
     * @param topic The topic of the channel.
     * @param users The users connected to the channel.
     */
    public Channel(Connection connection, String name, String topic, String[] users) {
        this.connection = connection;
        this.name = name;
        this.topic = topic;
        this.users = users;
    }

    public String getName() {
        return name;
    }

    @Override
    public void sendChat(String message) {
        connection.sendChat(name, message);
    }

    public String getTopic() {
        return topic;
    }

    public String[] getUsers() {
        return users.clone();
    }

    /**
     * Gets the number of users connected to this channel.
     */
    public int getUserCount() {
        return users.length;
    }

    /**
     * Gets a nickname of a connected user,OMITTING privilege prefixes (@, %, +)
     * @param i The index of the user.
     */
    public String getUser(int i) {
        if (users[i].matches("^[@%+]")) {
            return users[i].substring(1);
        }
        return users[i];
    }

    /**
     * Determines whether the user has operator privileges in the channel.
     *
     * @param i The index of the user.
     */
    public boolean isOp(int i) {
        return users[i].startsWith("@");
    }

    /**
     * Determines whether the user has half-op privileges in the channel.
     *
     * <br> The result of {@link #isOp(int)} carry over to this method.
     * @param i The index of the user.
     */
    public boolean isHalfOp(int i) {
        return users[i].startsWith("%") || isOp(i);
    }

    /**
     * Determines whether the user has voice privileges in the channel.
     *
     * <br>The results of {@link #isOp(int)} and {@link #isHalfOp(int)} carry over to this method.
     * @param i The index of the user.
     */
    public boolean isVoiced(int i) {
        return users[i].startsWith("+") || isHalfOp(i);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Channel)) return false;

        Channel channel = (Channel) o;

        return connection.equals(channel.connection)
                && name.equals(channel.name);

    }

    @Override
    public int hashCode() {
        int result = connection.hashCode();
        result = 31 * result + name.hashCode();
        return result;
    }
}
