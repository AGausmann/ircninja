/*
 * ircninja
 * Copyright (C) 2016  Adam Gausmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ninja.nonemu.irc;

/**
 * Provides utility methods related to the IRC protocol.
 *
 * The most common utilities are matchers that check if a string is formatted correctly as a nickname, hostname, etc.
 * as defined by <a href=https://tools.ietf.org/html/rfc2812>The IRC Client Protocol, RFC 2812</a>. There are also
 * matchers that check for and/or strip characters that are not allowed in the IRC protocol.
 */
public final class IrcUtil {
    private static final String nicknamePattern = "[A-Za-z\\[\\]\\\\`_^{|][A-Za-z0-9\\[\\]\\\\`_^{|}]*";
    private static final String usernamePattern = "[^\r\n @]+";
    private static final String hostPattern = "("
            + "[A-Za-z0-9]+(-[A-Za-z0-9]+)*(\\.[A-Za-z0-9]+(-[A-Za-z0-9]+)*)*" // Hostname
            + "|[0-9]{1,3}(\\.[0-9]{1,3}){3}" // Real IPv4
            + "|[0-9A-Fa-f]+(:[0-9A-Fa-f]+){7}" // Real IPv6
            + "|0:0:0:0:0:0:(0|FFFF):[0-9]{1,3}(\\.[0-9]{1,3}){3}" // IPv4 in IPv6 format
            + ")";

    private IrcUtil() {
    }

    /**
     * Checks to see if a string is a valid nickname.
     * This does NOT check whether the name exists in any connected server!
     *
     * <br>According to RFC 2812, valid nicknames are alphanumeric with some special characters [ ] \ ` _ ^ { | }, must
     * not start with a number, and must be 9 characters or less. Since some servers ignore the length
     * restriction, this method validates all checks except the length.
     *
     * @param s The string to check.
     * @return true if the string is a valid nickname, false otherwise.
     */
    public static boolean isNickname(String s) {
        return s.matches("^" + nicknamePattern + "$");
    }

    /**
     * Checks to see if a string is a valid username.
     * This does NOT check whether the name exists in any connected server!
     *
     * <br>According to RFC 2812, valid usernames contain any characters except carriage return, line feed, space and
     * \@ (the "at" symbol) and are of any length.
     * @param s The string to check.
     * @return true if the string is a valid username; false otherwise.
     */
    public static boolean isUsername(String s) {
        return s.matches("^" + usernamePattern + "$");
    }

    /**
     * Checks to see if a string is a valid user identifier.
     * This does NOT check whether the user exists in any connected server!
     *
     * <br>According to RFC 2812, user IDs are composed of a nickname optionally followed by a "!" with the user's
     * username and a "\@" followed by the host that the user is connected to.
     *
     * @param s The string to check.
     * @return true if the string is a valid user ID; false otherwise.
     */
    public static boolean isUserId(String s) {
        return s.matches("^" + nicknamePattern + "(!" + usernamePattern + "@" + hostPattern + ")?$");
    }

    /**
     * Checks to see if a string is a valid host.
     * This does NOT check whether the host exists in any connected server!
     *
     * <br>This method will accept hostnames as well as ipv4 and ipv6 addresses.
     * @param s The string to check.
     * @return true if the string is a valid host; false otherwise.
     */
    public static boolean isHost(String s) {
        return s.matches("^" + hostPattern + "$");
    }

    /**
     * Checks to see if a string is a valid channel name.
     * This does NOT check whether the channel exists in any connected server!
     *
     * <br> According to RFC 2812, valid channel names may start with # + &amp; and may consist of any characters except
     * ^G (control-G; ASCII bell), \r (carriage return), \n (line feed), space, comma, and colon.
     * There are also channel names which start with a "!" followed by 5 alphanumeric characters that compose the
     * channel ID. The rest of the name follows as usual after the ID.
     *
     * @param s The string to check.
     * @return True if the string is a valid channel name; false otherwise.
     */
    public static boolean isChannel(String s) {
        return s.matches("^[#+&][^\u0007\r\n ,:]+$") // Standard channels
                || s.matches("^![A-Za-z0-9]{5}[^\u0007\r\n ,:]+$"); // Channels with ID
    }

    /**
     * Checks to see if a string is a valid list of channels.
     * <br>This method splits the string by comma and determines whether each entry is a channel using
     * {@link #isChannel(String)}. No whitespace is allowed next to the comma. Single channels are also allowed.
     * @param s The string to check.
     * @return True if the string is a valid channel list; false otherwise;.
     */
    public static boolean isChannelList(String s) {
        String[] channels = s.split(",");
        for (String channel : channels) {
            if (!isChannel(channel)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks to see if a string contains characters which are "unsafe" in the IRC protocol.
     *
     * <br>According to RFC 2812, unsafe characters include ^G (ASCII bell), \r (carriage return), and \n (line feed).
     *
     * @param s The string to check.
     * @return True if the string contains unsafe characters; false otherwise.
     */
    public static boolean isUnsafe(String s) {
        return s.matches("[\u0007\r\n]");
    }

    /**
     * Escapes unsafe characters to more safe versions.
     *
     * <br>Any \n or \r\n sequence is escaped to a single space " " and ASCII bell characters are escaped to "^G".
     * @param original The original string.
     * @return The escaped string.
     */
    public static String escape(String original) {
        return original.replaceAll("\r?\n", " ").replaceAll("\u0007", "^G");
    }
}
