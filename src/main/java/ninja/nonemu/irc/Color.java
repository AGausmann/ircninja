/*
 * ircninja
 * Copyright (C) 2016  Adam Gausmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ninja.nonemu.irc;

import java.util.Arrays;
import java.util.regex.Pattern;

/**
 * A utility for parsing and converting colors to and from Java objects, IRC codes, and ANSI codes.
 */
public enum Color {
    WHITE("00", new int[] {37, 1}),
    BLACK("01", new int[] {30}),
    BLUE("02", new int[] {34}),
    GREEN("03", new int[] {32}),
    LIGHT_RED("04", new int[] {31, 1}),
    RED("05", new int[] {31}),
    PURPLE("06", new int[] {35}),
    ORANGE("07", new int[] {33}),
    YELLOW("08", new int[] {33, 1}),
    LIGHT_GREEN("09", new int[] {32, 1}),
    CYAN("10", new int[] {36}),
    LIGHT_CYAN("11", new int[] {36, 1}),
    LIGHT_BLUE("12", new int[] {34, 1}),
    PINK("13", new int[] {35, 1}),
    GREY("14", new int[] {30, 1}),
    LIGHT_GREY("15", new int[] {37}),
    DEFAULT("", new int[] {39});

    /**
     * A pattern that matches IRC color codes.
     */
    public static final Pattern IRC_COLOR_PATTERN = Pattern.compile("\u0003(?:([0-9]{1,2})(?:,([0-9]{1,2}))?)?|[\u0002\u001D\u001F\u0016\u000F]");

    /**
     * A pattern that matches ANSI color codes.
     */
    public static final Pattern ANSI_COLOR_PATTERN = Pattern.compile("\u001B\\[([0-9]+)(;[0-9]+)*m");

    private final String ircCode;
    private final int[] ansiCodes;

    Color(String ircCode, int[] ansiCodes) {
        this.ircCode = ircCode;
        this.ansiCodes = ansiCodes;
    }

    /**
     * Gets the code used to generate the IRC format string.
     * This should NOT be placed directly into text when formatting! See {@link #toIrc()} if you wish to do that.
     */
    public String getIrcCode() {
        return ircCode;
    }

    /**
     * Gets the code used to generate the ANSI format string.
     * This should NOT be placed directly into text when formatting! See {@link #toAnsi()} if you wish to do that.
     */
    public int[] getAnsiCodes() {
        return ansiCodes.clone();
    }

    /**
     * Converts the color's IRC code into the actual format string that is placed in text.
     * @return The generated IRC format string.
     */
    public String toIrc() {
        return "\u0003" + ircCode;
    }

    /**
     * Converts the color's ANSI code into the actual format string that is placed in text.
     * @return The generated ANSI format string.
     */
    public String toAnsi() {
        StringBuilder builder = new StringBuilder();

        builder.append("\u001B[");
        for (int i = 0; i < ansiCodes.length; i++) {
            builder.append(ansiCodes[i]);
            if (i != ansiCodes.length - 1) {
                builder.append(";");
            }
        }
        builder.append("m");

        return builder.toString();
    }

    @Override
    public String toString() {
        return toIrc();
    }

    /**
     * Gets the color that is associated with a given IRC code after parsing the IRC format string.
     * @param ircCode The IRC code.
     * @return The color associated with the code.
     */
    public static Color valueOfIrcCode(String ircCode) {
        for (Color color : values()) {
            if (color.ircCode.equals(ircCode)) {
                return color;
            }
        }
        return null;
    }

    /**
     * Gets the color that is associated with a given ANSI code after parsing the IRC format string.
     * @param ansiCodes The ANSI code.
     * @return The color associated with the code.
     */
    public static Color valueOfAnsiCode(int[] ansiCodes) {
        for (Color color : values()) {
            if (Arrays.equals(color.ansiCodes, ansiCodes)) {
                return color;
            }
        }
        return null;
    }

    /**
     * Utility that strips IRC format strings from a given string.
     * @param s The string to strip.
     * @return A string with the same text but no colors.
     */
    public static String stripColors(String s) {
        return s.replaceAll(IRC_COLOR_PATTERN.pattern(), "");
    }

    /**
     * Utility that generates an IRC foreground and background color change.
     * Only foreground is changed when using {@link #toIrc()}
     * @param foreground The foreground color.
     * @param background The background color.
     * @return The generated IRC format string.
     */
    public static String toIrc(Color foreground, Color background) {
        return foreground.toIrc() + "," + background.getIrcCode();
    }

    /**
     * Utility that generates an ANSI foreground and background color change.
     * Only foreground is changed when using {@link #toAnsi()}
     * @param foreground The foreground color.
     * @param background The background color.
     * @return The generated ANSI format string.
     */
    public static String toAnsi(Color foreground, Color background) {
        StringBuilder builder = new StringBuilder();
        builder.append(foreground.toAnsi().substring(0, builder.length() - 1));

        for (int ansiCode : background.ansiCodes) {
            if (ansiCode >= 30 && ansiCode < 40) {
                ansiCode += 10;
            }
            builder.append(";").append(ansiCode);
        }
        builder.append("m");
        return builder.toString();
    }
}
