/*
 * ircninja
 * Copyright (C) 2016  Adam Gausmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ninja.nonemu.irc;

/**
 * Represents a user which is currently online. Because more information is available about online users, this class
 * is constructed to extend User and encapsulate that extra information.
 *
 * For handling users that may not be online currently, use {@link User}.
 *
 * @see User
 * @see Connection#getUser(String)
 */
public class OnlineUser extends User {
    private final boolean operator;
    private final int idle;
    private final String[] channels;

    /**
     * Constructs a new online user instance.
     * @param connection The connection that the user originated from.
     * @param nick The nickname of the user.
     * @param user The username of the user.
     * @param host The hostname of the user.
     * @param realname The real name of the user.
     * @param server The server the user is connected to.
     * @param operator Whether the user is an operator.
     * @param idle How long the user has been idle, in seconds.
     * @param channels A list of channels the user is connected to, including privilege prefixes (@, %, +)
     */
    public OnlineUser(Connection connection, String nick, String user, String host, String realname, String server, boolean operator, int idle, String[] channels) {
        super(connection, nick, user, host, realname, server, true);

        this.operator = operator;
        this.idle = idle;
        this.channels = channels;
    }

    public boolean isOperator() {
        return operator;
    }

    public int getIdle() {
        return idle;
    }

    public String[] getChannels() {
        return channels.clone();
    }

    /**
     * Gets the number of channels that the user has connected to.
     */
    public int getChannelCount() {
        return channels.length;
    }

    /**
     * Gets the name of the specified channel, OMITTING any privilege prefixes.
     * @param i The index of the channel.
     */
    public String getChannel(int i) {
        if (channels[i].matches("^[@+%]")) {
            return channels[i].substring(1);
        }
        return channels[i];
    }

    /**
     * Determines whether the user has operator privileges in the channel.
     *
     * @param i The index of the channel.
     */
    public boolean isOpOn(int i) {
        return channels[i].startsWith("@");
    }

    /**
     * Determines whether the user has half-op privileges in the channel.
     *
     * <br> The result of {@link #isOpOn(int)} carry over to this method.
     * @param i The index of the channel.
     */
    public boolean isHalfOpOn(int i) {
        return channels[i].startsWith("%") || isOpOn(i);
    }

    /**
     * Determines whether the user has voice privileges in the channel.
     *
     * <br>The results of {@link #isOpOn(int)} and {@link #isHalfOpOn(int)} carry over to this method.
     * @param i The index of the channel.
     */
    public boolean isVoicedOn(int i) {
        return channels[i].startsWith("+") || isHalfOpOn(i);
    }
}
