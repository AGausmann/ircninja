/*
 * ircninja
 * Copyright (C) 2016  Adam Gausmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ninja.nonemu.irc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Instances of this class establish connections to IRC networks.
 *
 * <br>It is unsafe and not recommended to instantiate this class directly unless you know what you are doing. Use the
 * {@link ConnectionBuilder} class to build an instance instead, as it performs more checks.
 *
 * @see ConnectionBuilder
 */
public class Connection {
    private final String host;
    private final int port;
    private final String username;
    private final String realname;
    private final String password;
    private final Queue<Message> messageQueue;

    private String nickname;
    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;

    /**
     * Constructs a new connection using complete details.
     * @param host The hostname of the IRC network; cannot be null.
     * @param port The port of the IRC network, usually 6667.
     * @param nickname The nickname to be associated with the client; cannot be null.
     * @param username The username to be associated with the client; cannot be null.
     * @param realname The real name to be associated with the client; cannot be null.
     * @param password The password to authenticate to the network; can be null or empty if not needed.
     */
    public Connection(String host, int port, String nickname, String username,
                      String realname, String password) {
        this.host = host;
        this.port = port;
        this.nickname = nickname;
        this.username = username;
        this.realname = realname;
        this.password = password;
        messageQueue = new LinkedList<>();

        socket = null;
        in = null;
        out = null;
    }

    private String receiveRaw() throws IOException {
        return in.readLine();
    }

    private void sendRaw(String message) {
        out.print(message + "\r\n");
        out.flush();
    }

    private Message receiveMessage() throws IOException {
        return Message.decode(receiveRaw());
    }

    /**
     * Approximates whether the connection will promptly be able to read a message.
     * <br>The accuracy of this method depends on the server that is sending messages; if the server sends part of a
     * message before the rest, the connection may need some extra time to read the message.
     * @return true if the connection believes it can read a message; false otherwise.
     * @throws IOException if an I/O error occurs.
     */
    public boolean canReadMessage() throws IOException {
        return (in != null && in.ready()) || !messageQueue.isEmpty();
    }

    /**
     * Reads a message sent by the server.
     * @return The message read.
     * @throws IOException if an IO error occurs while reading.
     */
    public Message readMessage() throws IOException {
        Message message;
        if (messageQueue.isEmpty()) {
            message = receiveMessage();
        } else {
            message = messageQueue.remove();
        }

        if (message.getCommand().equalsIgnoreCase("PING")) {
            sendMessage(new Message(null, "PONG", message.getArgs()));
        } else if (message.getCommand().equalsIgnoreCase("NICK")
                && message.senderName().equalsIgnoreCase(nickname)
                && message.getArgCount() > 0) {
            nickname = message.getArg(0);
        }

        return message;
    }

    /**
     * Sends a custom message to the server.
     * @param message The message to send.
     */
    public void sendMessage(Message message) {
        if (!message.isValid()) {
            throw new IllegalArgumentException("Message is not formatted correctly");
        }

        sendRaw(message.encode());
    }

    /**
     * Checks whether a connection exists by checking if there is a socket and it says it is connected.
     * @return true if there is a connection; false otherwise.
     */
    public boolean isConnected() {
        return socket != null && socket.isConnected() && !socket.isClosed();
    }

    /**
     * Connects to the network using the details provided during construction.
     *
     * <br>This may send any/all of the following: PASS, NICK, and USER messages.
     *
     * @throws IOException if an IO error occurs while connecting.
     */
    public void connect() throws IOException {
        if (isConnected()) {
            throw new IllegalStateException("Connection already established");
        }

        socket = new Socket(host, port);
        in = new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF-8"));
        out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "UTF-8"));

        if (password != null && !password.isEmpty()) {
            sendRaw(String.format("PASS %s", password));
        }

        setNickname(nickname);
        sendRaw(String.format("USER %s 0 * :%s", username, realname));
    }

    /**
     * Disconnects from any connected network.
     *
     * @param comment The comment to leave when disconnecting.
     * @throws IOException if an IO error occurs while disconnecting.
     */
    public void disconnect(String comment) throws IOException {
        if (!isConnected()) {
            throw new IllegalStateException("No connection established");
        }
        if (IrcUtil.isUnsafe(comment)) {
            throw new IllegalArgumentException("Illegal characters in comment");
        }

        sendRaw(String.format("QUIT %s", comment));
        socket.close();

        in = null;
        out = null;
        socket = null;
    }

    /**
     * Disconnects from any connected network without providing a comment.
     *
     * @throws IOException if an IO error occurs while disconnecting.
     */
    public void disconnect() throws IOException {
        disconnect("No comment");
    }

    /**
     * Attempts to set a new nickname for the client.
     *
     * @param nickname The new nickname.
     */
    public void setNickname(String nickname) {
        if (!IrcUtil.isNickname(nickname)) {
            throw new IllegalArgumentException("Invalid characters in nickname");
        }

        sendRaw(String.format("NICK %s", nickname));
    }

    /**
     * Attempts to join a channel.
     *
     * @param channel The channel (or list of channels) to join.
     */
    public void joinChannel(String channel) {
        if (!IrcUtil.isChannelList(channel)) {
            throw new IllegalArgumentException("Invalid characters in channel");
        }

        sendRaw(String.format("JOIN %s", channel));
    }

    /**
     * Attempts to leave a channel, leaving a comment behind.
     * @param channel The channel to leave.
     * @param comment The comment/reason for leaving.
     */
    public void leaveChannel(String channel, String comment) {
        if (!IrcUtil.isChannel(channel)) {
            throw new IllegalArgumentException("Invalid characters in channel");
        }
        if (IrcUtil.isUnsafe(comment)) {
            throw new IllegalArgumentException("Invalid characters in comment");
        }

        sendRaw(String.format("PART %s :%s", channel, comment));
    }

    /**
     * Attempts to leave a channel with no comment.
     * @param channel The channel to leave.
     */
    public void leaveChannel(String channel) {
        leaveChannel(channel, "No comment");
    }

    /**
     * Sends a chat message to a channel or user connected to the network.
     * @param target The entity receiving the message.
     * @param message The message to send.
     */
    public void sendChat(String target, String message) {
        if (!IrcUtil.isUserId(target) && !IrcUtil.isChannel(target)) {
            throw new IllegalArgumentException("Bad target format: must be a user or channel");
        }
        if (message.contains("\u0007")) {
            throw new IllegalArgumentException("Message contains illegal characters");
        }

        String[] lines = message.split("\r?\n");
        for (String line : lines) {
            sendRaw(String.format("PRIVMSG %s :%s", target, line));
        }
    }

    public Channel getChannel(String channel) throws IOException {
        if (!IrcUtil.isChannel(channel)) {
            throw new IllegalArgumentException("Channel name is illegally formatted.");
        }

        String topic = null;
        String[] users = new String[0];

        sendRaw(String.format("TOPIC %s", channel));

        // Prevents an infinite loop in case the data is never received.
        for (int i = 0; i < 18; i++) {
            Message message = receiveMessage();

            if (message.getCommand().equals("331") // RPL_NOTOPIC
                    && message.getArg(1).equalsIgnoreCase(channel)) {
                topic = null;
                break;

            } else if (message.getCommand().equals("332") // RPL_TOPIC
                    && message.getArg(1).equalsIgnoreCase(channel)) {
                topic = message.getArg(2);
                break;

            } else {
                messageQueue.add(message);
            }
        }

        sendRaw(String.format("NAMES %s", channel));

        for (int i = 0; i < 18; i++) {
            Message message = receiveMessage();

            if (message.getCommand().equals("353")  // RPL_NAMREPLY
                    && message.getArg(2).equalsIgnoreCase(channel)) {
                users = message.getArg(3).split(" ");

            } else if (message.getCommand().equals("366")) { // RPL_ENDOFNAMES
                break;

            } else {
                messageQueue.add(message);
            }
        }

        return new Channel(this, channel, topic, users);
    }

    /**
     * Gets information about a user who is or was connected to an IRC network.
     *
     * <br>If the user is currently online, then an instance of {@link OnlineUser} is returned. If the user is offline
     * but has visited the network, then an instance of {@link User} is returned. If neither of the conditions are met,
     * then null is returned.
     * @param userId The user mask, which may be the format {@code nick} or {@code nick!user@host}
     * @return An instance of User or OnlineUser, or null if the user is unknown.
     * @throws IOException if an IO error occurs.
     */
    public User getUser(String userId) throws IOException {
        if (!IrcUtil.isUserId(userId)) {
            throw new IllegalArgumentException("User ID is illegally formatted.");
        }

        sendRaw(String.format("WHOIS %s", userId));

        String nick = null;
        String user = null;
        String host = null;
        String realname = null;
        String server = null;
        boolean operator = false;
        int idle = 0;
        String[] channels = new String[0];

        // Prevents an infinite loop in case the data is never received.
        for (int i = 0; i < 22; i++) {
            Message message = receiveMessage();
            if (message.getCommand().equals("311")) { // RPL_WHOISUSER
                nick = message.getArg(1);
                user = message.getArg(2);
                host = message.getArg(3);
                realname = message.getArg(4);

            } else if (message.getCommand().equals("312")) { // RPL_WHOISSERVER
                server = message.getArg(2);

            } else if (message.getCommand().equals("313")) { // RPL_WHOISOPERATOR
                operator = true;

            } else if (message.getCommand().equals("317")) { // RPL_WHOISIDLE
                idle = Integer.parseInt(message.getArg(2));

            } else if (message.getCommand().equals("318")) { // RPL_ENDOFWHOIS
                break;

            } else if (message.getCommand().equals("319")) { // RPL_WHOISCHANNELS
                channels = message.getArg(2).split(" ");

            } else if (message.getCommand().equals("401")) { // ERR_NOSUCHNICK
                return whowasUser(userId);
            } else {
                messageQueue.add(message);
            }
        }

        return new OnlineUser(this, nick, user, host, realname, server, operator, idle, channels);
    }

    /**
     * Utility method for getting offline user data. Should not be exposed except as a part of {@link #getUser(String)}
     */
    private User whowasUser(String userId) throws IOException {
        sendRaw(String.format("WHOWAS %s", userId));

        String nick = null;
        String user = null;
        String host = null;
        String realname = null;
        String server = null;

        // Prevents an infinite loop in case the data is never received.
        for (int i = 0; i < 19; i++) {
            Message message = receiveMessage();
            if (message.getCommand().equals("312")) { // RPL_WHOISSERVER
                server = message.getArg(2);

            } else if (message.getCommand().equals("314")) { // RPL_WHOWASUSER
                nick = message.getArg(1);
                user = message.getArg(2);
                host = message.getArg(3);
                realname = message.getArg(4);

            } else if (message.getCommand().equals("369")) { // RPL_ENDOFWHOWAS
                break;

            } else if (message.getCommand().equals("406")) { // ERR_WASNOSUCHNICK
                return null;
            } else {
                messageQueue.add(message);
            }
        }

        return new User(this, nick, user, host, realname, server, false);
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public String getUsername() {
        return username;
    }

    public String getRealname() {
        return realname;
    }

    public String getNickname() {
        return nickname;
    }
}
